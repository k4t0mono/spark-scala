package wtf.stuffium.sec1

import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.SparkContext

object UniqueWords {
    def main(args: Array[String]) {
        Logger.getLogger("org").setLevel(Level.ERROR);

        val sc = new SparkContext("local[*]", "UniquesWords")
        val input = sc.textFile("data/ls02_body_play.txt")
        
        val words = input.flatMap(x => x.split("\\W+"))

        val lowercase_words = words.map(x => x.toLowerCase())
        val words_counts = lowercase_words
            .map(x => (x, 1))
            .reduceByKey((x,y) => x + y)

        val words_sorted = words_counts
            .map(x => (x._2, x._1))
            .sortByKey()

        words_sorted.foreach(println)
    }
}