package wtf.stuffium.sec1

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import math.{min, max}

object MinTemps {

	def parseLines(line: String): (String, String, Float) = {
		val fields = line.split(",")
		val station_id = fields(0)
		val entry_type = fields(2)
		val data = fields(3).toFloat
		(station_id, entry_type, data)
	}

	def getMinTemp(rdd: RDD[(String, String, Float)]): Array[(String, Float)] = {
		val min_temps = rdd.filter(x => x._2 == "TMIN")
		val station_temps = min_temps.map(x => (x._1, x._3 * 0.1f))
		val min_temp_station = station_temps.reduceByKey((x,y) => min(x, y))

		min_temp_station.collect()
	}

	def getMaxTemp(rdd: RDD[(String, String, Float)]): Array[(String, Float)] = {
		val min_temps = rdd.filter(x => x._2 == "TMAX")
		val station_temps = min_temps.map(x => (x._1, x._3 * 0.1f))
		val min_temp_station = station_temps.reduceByKey((x,y) => max(x, y))

		min_temp_station.collect()
	}

	def getMaxPrcp(rdd: RDD[(String, String, Float)]): Array[(String, Float)] = {
		val prcp = rdd.filter(x => x._2 == "PRCP")
		val station_prcp = prcp.map(x => (x._1, x._3 * 0.01f))
		val max_prcp_station = station_prcp.reduceByKey((x, y) => max(x, y))

		max_prcp_station.collect()
	}

	def main(args: Array[String]): Unit = {
		Logger.getLogger("org").setLevel(Level.ERROR)

		val sc = new SparkContext(master="local[*]", appName = "MinTemps")
		val lines = sc.textFile("./data/1800.csv")
		val rdd = lines.map(parseLines)

		getMinTemp(rdd).sorted.foreach(x => {
			println(s"${x._1} minimum temperature: ${x._2} C")
		})

		getMaxTemp(rdd).sorted.foreach(x => {
			println(s"${x._1} maximum temperature: ${x._2} C")
		})

		getMaxPrcp(rdd).sorted.foreach(x => {
			println(s"${x._1} maximum preciptaion: ${x._2} mm")
		})
	}

}
