package wtf.stuffium.sec1

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object  CostumerPurchases {
    def parseLine(line: String): (Int, Float) = {
        val fields = line.split(",")
        val user = fields(0).toInt
        val amount = fields(2).toFloat

        (user, amount)
    }

    def main(args: Array[String]) {
        Logger.getLogger("org").setLevel(Level.ERROR);

        val sc = new SparkContext("local[*]", "UniquesWords")
        val input = sc.textFile("data/customer-orders.csv")
        val rdd = input.map(parseLine)

        val user_total = rdd
            .reduceByKey((x,y) => x + y)
            .map(x => (x._2, x._1))
            .sortByKey(false)
            .collect()

        user_total.foreach(x => { println(s"${x._2}: ${x._1}") })
    }
}