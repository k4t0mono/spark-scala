package wtf.stuffium.sec2

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext

object PopularMovie {
  def parseLine(line: String): (Int, Int, Int, Int) = {
    val List(user, movie, rating, time) = line.split("\t").toList

    (user.toInt, movie.toInt, rating.toInt, time.toInt)
  }

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.ERROR)

    val sc = new SparkContext("local[*]", "PopularMovie")
    val lines = sc.textFile("data/ml-100k/u.data")

    val movies = lines
      .map(parseLine)
      .map(x => (x._2, 1))

    val movie_counts = movies.reduceByKey((x, y) => x + y)
    val results = movie_counts
      .map(x => (x._2, x._1))
      .sortByKey()
      .collect()

    results.foreach(println)
  }
}
