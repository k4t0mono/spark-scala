package wtf.stuffium.sec2

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext

import scala.io.{Codec, Source}

object PopularMovieNames {

  def parseMovieNames() : Map[Int, String] = {
    var movieNames:Map[Int, String] = Map()

    val source = Source.fromFile("data/ml-100k/u.item")
    val lines = source.getLines()

    for(ln <- lines) {
      val fields = ln.split('|')
      if(fields.nonEmpty) {
        movieNames += (fields(0).toInt -> fields(1))
      }
    }
    source.close()

    movieNames
  }

  def parseLine(line: String): (Int, Int, Int, Int) = {
    val List(user, movie, rating, time) = line.split("\t").toList

    (user.toInt, movie.toInt, rating.toInt, time.toInt)
  }

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.ERROR)

    val sc = new SparkContext("local[*]", "parseMovieNames")
    val nameDict = sc.broadcast(parseMovieNames())
    val lines = sc.textFile("data/ml-100k/u.data")

    val movies = lines
      .map(parseLine)
      .map(x => (x._2, 1))

    val movie_counts = movies.reduceByKey((x, y) => x + y)
    val results = movie_counts
      .map(x => (x._2, x._1))
      .sortByKey()
      .map(x => (nameDict.value(x._2), x._1))
      .collect()

    results.foreach(println)
  }
}

